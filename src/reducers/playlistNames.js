const playlistNamesDefault = [];

export default (state = playlistNamesDefault, action) => {
    switch(action.type) {
        case 'FETCH_PLAYLIST_NAMES':
            return [...state, ...action.playlistNames];
        case 'RESET_STORE':
            return [];
        default:
            return state;
    }
};