const playlistsDefault = [];

export default (state = playlistsDefault, action) => {
    //console.log('action in reducer', action);
    //console.log('state in reducer', state);
    switch(action.type) {
        case 'FETCH_PLAYLISTS_FROM_DB':
            return [...state, ...action.playlistsDb];
        case 'RESET_STORE':
            return [];
        default:
            return state;
    }
};