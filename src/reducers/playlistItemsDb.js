const playlistItemsDefault = [];

export default (state = playlistItemsDefault, action) => {
    //  console.log('state in reducer', state);
    //  console.log('action in reducer', action);
    switch(action.type) {
        case 'FETCH_PLAYLIST_ITEMS_FROM_DB':
            return [
                ...state, 
                {
                    playlistId: action.playlistId,
                    playlistItems: action.playlistItems
                }
            ];
        case 'RESET_STORE':
            return [];
        default:
            return state;
    }
};