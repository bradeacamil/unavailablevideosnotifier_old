const playlistsDefault = [];

export default (state = playlistsDefault, action) => {
    //console.log('action in reducer', action);
    //console.log('state in reducer', state);
    switch(action.type) {
        case 'FETCH_PLAYLISTS_FROM_YOUTUBE':
            return [...state, ...action.playlistsYoutube];
        case 'RESET_STORE':
            return [];
        default:
            return state;
    }
};