import React from 'react';
import ReactDOM from 'react-dom';

import { withStyles } from 'material-ui/styles';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';

const Track = (props) => (
    <div style={paddingLeftRight}>
        {props.track && 
            <div style={paddingTopBottom}>
                <Typography>
                    {props.track}
                </Typography>
                <Divider light />
            </div>
        }
        
        {props.youtubeTrack && 
            <div style={paddingTopBottom}>
                <Typography>
                    Youtube: {props.youtubeTrack}
                </Typography>
                <Typography>
                    What you saved: {props.dbTrack}
                </Typography>
                <Divider light />
            </div>
        }

        {(props.previousTrack || props.nextTrack) && 
            <div style={paddingTopBottom}>
                <Typography style={paddingTopBottom}>
                    Previous track: {props.previousTrack}
                </Typography>
                <Typography>
                    Next track: {props.nextTrack}
                </Typography>
                <div style={paddingTopBottom} />
                <Divider light />
            </div>
        }
    </div>
);

const paddingLeftRight = {
    paddingLeft: 20,
    paddingRight: 20
}

const paddingTopBottom = {
    paddingBottom: 20
}

export default Track;