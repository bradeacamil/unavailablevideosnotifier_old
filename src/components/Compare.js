import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import diff from 'deep-diff';
import _ from 'lodash';
import { GoogleLogin } from 'react-google-login';

import ShowDifference from './ShowDifference';
import ShowWarning from './ShowWarning';
import { 
    getPlaylistIdsFromYoutube, 
    getTracksFromYoutube,
    getPlaylistIdsFromDb,
    getTracksFromDb,
    getPlaylistNames,
    writePlaylistsToDb,
    writeTracksToDb
} from '../utils/helpers';
import { 
    fetchPlaylistsFromYoutube, 
    fetchPlaylistsFromDb,
    fetchPlaylistNames
} from '../actions/playlists';
import { 
    fetchPlaylistItemsFromYoutube, 
    fetchPlaylistItemsFromDb 
} from '../actions/playlistItems';

class Compare extends React.Component {
    state = {
        loading: false,
        clickedCompare: false,
        differences: [],
        warnings: [],
        channel: ''
    };

    setInitialState = () => {
        const initialState = {
            loading: false,
            clickedCompare: false,
            differences: [],
            warnings: []
        };
        return initialState;
    }

    componentWillUnmount = () => {
        this.props.dispatch({
            type: 'RESET_STORE'
        });

        this.setState(() => this.setInitialState());
        this.showDifferences();
        this.showWarnings();
    }

    fetch = () => {
        let i = 0;

        getPlaylistNames(this.state.channel)
            .then((result) => this.props.dispatch(fetchPlaylistNames(result)));

        return new Promise((resolve, reject) =>  {
            getPlaylistIdsFromDb(this.state.channel)
            .then((result) => {
                this.props.dispatch(fetchPlaylistsFromDb(result.sort()));

                this.props.playlistsDb.map((playlistId) => {
                    getTracksFromDb(playlistId)
                        .then((playlistItems) => {
                            this.props.dispatch(fetchPlaylistItemsFromDb(playlistId, playlistItems));
                        });
                });

                getPlaylistIdsFromYoutube(this.state.channel)
                .then((result) => {
                    this.props.dispatch(fetchPlaylistsFromYoutube(result.sort()));
    
                    this.props.playlistsYoutube.map((playlistId) => {
                        getTracksFromYoutube(playlistId)
                            .then((playlistItems) => {
                                this.props.dispatch(fetchPlaylistItemsFromYoutube(playlistId, playlistItems));
    
                                i++;
                                if (i === this.props.playlistsYoutube.length)
                                    resolve();
                            });
                    });
                });
            })
        });
    }

    getPlaylistName = (playlistId) => {
        let result = '';
        
        this.props.playlistNames.map((playlist) => {
            if (playlist.playlistId.trim(' ') === playlistId.trim(' '))
                result = playlist.playlistName;
        });

        return result;
    }

    onChannelChange = (e) => {
        const channel = e.target.value.trim(' ');
        this.setState(() => ({channel}));
    }

    onCompareClick = () => {
        let differences = [];
        const tracksYoutube = [];
        const tracksDb = [];

        this.setState(() => ({ loading: true }));
        
        this.fetch()
            .then(() => {
                this.setState(() => ({ loading: false }));

                let sortedYoutube = _.sortBy(this.props.playlistItemsYoutube, [(criteria) => criteria.playlistId ]);
                let sortedDb = _.sortBy(this.props.playlistItemsDb, [(criteria) => criteria.playlistId ]);
        
                this.setState((prevState) => ({ clickedCompare: true }));

                sortedYoutube.map((youtubeItem) => {
                    let dbItem = sortedDb.find((item) => item.playlistId === youtubeItem.playlistId );
                    
                    if (youtubeItem && dbItem) {
                        let differences = diff(youtubeItem, dbItem);

                        if (differences !== undefined) {
                            differences.map((difference) => {
                                if (difference.kind === 'E') {
                                    if (difference.lhs === 'Deleted video' || difference.lhs === 'Private video') {
                                        this.setState((prevState) => ({ differences: [...prevState.differences, {
                                                playlistId: youtubeItem.playlistId,
                                                deletedTrack: difference.rhs
                                            }] 
                                        }));
                                    }
                                    else {
                                        this.setState((prevState) => ({ warnings: [...prevState.warnings, {
                                                playlistId: youtubeItem.playlistId,
                                                youtubeTrack: difference.lhs,
                                                dbTrack: difference.rhs
                                            }] 
                                        }));
                                    }
                                }
                            });
                        }
                    }
                });
            })
            .catch((e) => console.log(e));
    }

    onNewCompareClick = () => {
        this.props.dispatch({
            type: 'RESET_STORE'
        });

        this.setState(() => this.setInitialState());
        this.showDifferences();
        this.showWarnings();
    }

    onSaveClick = () => {
        writePlaylistsToDb(this.state.channel, this.props.playlistsYoutube);
        this.props.playlistItemsYoutube.map((playlist) => {
            writeTracksToDb(playlist.playlistId, playlist.playlistItems);
        });
    }

    responseGoogle = (response) => {
        console.log(response);
    }

    showDifferences = () => {
        let result = [];
        let playlistsWithDeletedVideos = [];

        this.state.differences.map((playlist) => {
            if (!playlistsWithDeletedVideos.includes(playlist.playlistId))
                playlistsWithDeletedVideos.push(playlist.playlistId.trim(' '));
        });

        return playlistsWithDeletedVideos;
    }

    showWarnings = () => {
        let result = [];
        let playlistsWithWarnings = [];

        this.state.warnings.map((playlist) => {
            if (!playlistsWithWarnings.includes(playlist.playlistId)) {
                playlistsWithWarnings.push(playlist.playlistId.trim(' '));
            }
        });

        return playlistsWithWarnings;
    }

    render() {
        return (
            <div>
            <div>
            <GoogleLogin
                clientId="1002960524442-ma7hkh6supaaq4d83nerbuk7es7rb8bq.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={this.responseGoogle}
                onFailure={this.responseGoogle}
            />
            </div>
            <input
                type="text"
                placeholder="Enter channel ID ..."
                autoFocus
                value={this.props.channel}
                defaultValue='UC-2KsQGXHYO_1uA95pV4VjA'
                onChange={this.onChannelChange}
            />
            {(!this.state.clickedCompare && this.state.channel.length > 0) && <button onClick={this.onCompareClick}>Compare</button>}
            {(this.state.channel.length > 0 && !this.state.channel.match(/^[A-Za-z0-9_\-]{24}$/)) && <p>Wrong channel ID !</p>}
                {
                    this.state.loading ? 
                        <div>Fetching data ...</div>
                        :
                        <div>
                            
                            {this.state.clickedCompare && <button onClick={this.onNewCompareClick}>New comparison</button>}
                            {(this.state.clickedCompare) && <button onClick={this.onSaveClick}>Persist</button>}
                            {this.state.clickedCompare ?
                                <div>
                                    {                        
                                        this.showDifferences().map((playlistId) =>
                                            <ShowDifference 
                                                key={playlistId} 
                                                playlistId={playlistId} 
                                                playlistName={this.getPlaylistName(playlistId)} 
                                                differences={this.state.differences}
                                            />     
                                        )
                                    }
                                    <h3>Warnings: </h3>
                                    {
                                        this.showWarnings().map((playlistId) =>
                                            <ShowWarning 
                                                key={playlistId} 
                                                playlistId={playlistId} 
                                                playlistName={this.getPlaylistName(playlistId)} 
                                                warnings={this.state.warnings}
                                            />     
                                        )
                                    }
                                </div>
                                :
                                ''  
                            }   
                        </div>
                }
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    //console.log('state in mapstatetoprops', state);
    return {
        playlistsYoutube: state.playlistsYoutube,
        playlistItemsYoutube: state.playlistItemsYoutube,
        playlistsDb: state.playlistsDb,
        playlistItemsDb: state.playlistItemsDb,
        playlistNames: state.playlistNames
    };
};

export default connect(mapStateToProps)(Compare);