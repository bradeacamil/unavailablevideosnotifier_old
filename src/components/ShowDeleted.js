import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import classnames from 'classnames';
import Card, { CardHeader, CardMedia, CardContent, CardActions } from 'material-ui/Card';
import Collapse from 'material-ui/transitions/Collapse';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import PlaylistIcon from 'material-ui-icons/PlaylistPlay';
import Typography from 'material-ui/Typography';
import red from 'material-ui/colors/red';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';

import Track from './Track';

const styles = theme => ({
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    flexGrow: {
        flex: '1 1 auto',
    },
});

class ShowDeleted extends React.Component {
    state = { expanded: false };

    handleExpandClick = () => {
        this.setState({ expanded: !this.state.expanded });
    };

    getNumberOfDeletedVideos = () => {
        let result = 0;
        this.props.deletedVideos.map((track) => {
            if (this.props.playlistId === track.playlistId)
                result++;
        });

        return result;
    }

    render() {
        const { classes } = this.props;

        return (
            <div >
                <Card className={classes.card}>
                    <CardHeader
                        title={
                            <div style={header}>
                                <div>
                                    <Typography>
                                            {this.props.playlistName}
                                    </Typography>
                                </div>
                                <div>
                                    <IconButton aria-label="Go to playlist" href={`https://www.youtube.com/playlist?list=${this.props.playlistId}`}>
                                        <PlaylistIcon />
                                    </IconButton>
                                </div>
                            </div>
                        }
                        subheader={<Typography>{this.getNumberOfDeletedVideos() + ' deleted video(s)'}</Typography>}
                        style={insideMargin}
                    />

                    <CardActions disableActionSpacing>
                        <div className={classes.flexGrow} />
                        <IconButton
                            className={
                                classnames(classes.expand, {
                                    [classes.expandOpen]: this.state.expanded,
                                })
                            }
                            onClick={this.handleExpandClick}
                            aria-expanded={this.state.expanded}
                            aria-label="Show more"
                        >
                            <ExpandMoreIcon />
                        </IconButton>
                    </CardActions>

                    <Collapse in={this.state.expanded} transitionDuration="auto" unmountOnExit>
                        <CardContent>
                            <Typography paragraph type="body1">
                            {
                                this.props.deletedVideos.map((track) => {
                                    if (this.props.playlistId === track.playlistId) {
                                        return <Track 
                                                    key={track.previousTrack} 
                                                    previousTrack={track.previousTrack} 
                                                    nextTrack={track.nextTrack}
                                               />
                                    }
                                })
                            }
                            </Typography>
                        </CardContent>
                    </Collapse>
                </Card>
            </div>
        );
    }
}

ShowDeleted.propTypes = {
    classes: PropTypes.object.isRequired,
};

const paddingOutside = {
    paddingTop: 20,
    paddingRight: 35,
    paddingLeft: 35
}

const paddingInside = {
    paddingLeft: 20,
    paddingRight: 20
}

const insideMargin = {
    marginLeft: 15
}

const alignCenter = {
    textAlign: 'center'
}

const header = {
    display: 'flex',
    justifyContent: 'space-between'
}

export default withStyles(styles)(ShowDeleted);




// import React from 'react';
// import ReactDOM from 'react-dom';
// import PropTypes from 'prop-types';

// import { withStyles } from 'material-ui/styles';
// import classnames from 'classnames';
// import Card, { CardHeader, CardMedia, CardContent, CardActions } from 'material-ui/Card';
// import Collapse from 'material-ui/transitions/Collapse';
// import Avatar from 'material-ui/Avatar';
// import IconButton from 'material-ui/IconButton';
// import PlaylistIcon from 'material-ui-icons/PlaylistPlay';
// import Typography from 'material-ui/Typography';
// import red from 'material-ui/colors/red';
// import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
// import ExpansionPanel, {
//     ExpansionPanelSummary,
//     ExpansionPanelDetails,
// } from 'material-ui/ExpansionPanel';

// import Track from './Track';

// const styles = theme => ({
//     expand: {
//         transform: 'rotate(0deg)',
//         transition: theme.transitions.create('transform', {
//             duration: theme.transitions.duration.shortest,
//         }),
//     },
//     expandOpen: {
//         transform: 'rotate(180deg)',
//     },
//     flexGrow: {
//         flex: '1 1 auto',
//     },
// });

// class ShowDeleted extends React.Component {
//     state = { expanded: false };

//     handleExpandClick = () => {
//         this.setState({ expanded: !this.state.expanded });
//     };

//     getNumberOfDeletedVideos = () => {
//         let result = 0;
//         this.props.deletedVideos.map((track) => {
//             if (this.props.playlistId === track.playlistId)
//                 result++;
//         });

//         return result;
//     }

//     render() {
//         const { classes } = this.props;

//         return (
//             <div >
//                 <ExpansionPanel>
//                     <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
//                     <Typography className={classes.heading}>
//                         <div style={header}>
//                             <div>
//                                 <Typography>
//                                         {this.props.playlistName} - {this.getNumberOfDeletedVideos() + ' deleted video(s)'}
//                                 </Typography>
//                             </div>
//                             <div>
//                                 <IconButton aria-label="Go to playlist" href={`https://www.youtube.com/playlist?list=${this.props.playlistId}`}>
//                                     <PlaylistIcon />
//                                 </IconButton>
//                             </div>
//                         </div>
//                     </Typography>
//                 </ExpansionPanelSummary>
//                 <ExpansionPanelDetails>
//                     <div style={insideMargin}>
//                         <Typography paragraph type="body1">
//                         {
//                             this.props.deletedVideos.map((track) => {
//                                 if (this.props.playlistId === track.playlistId) {
//                                     return <Track 
//                                                 key={track.previousTrack} 
//                                                 previousTrack={track.previousTrack} 
//                                                 nextTrack={track.nextTrack}
//                                         />
//                                 }
//                             })
//                         }
//                         </Typography>
//                     </div>
//                 </ExpansionPanelDetails>
//                 </ExpansionPanel>
//             </div>
//         );
//     }
// }

// ShowDeleted.propTypes = {
//     classes: PropTypes.object.isRequired,
// };

// const paddingOutside = {
//     paddingTop: 20,
//     paddingRight: 35,
//     paddingLeft: 35
// }

// const paddingInside = {
//     paddingLeft: 20,
//     paddingRight: 20
// }

// const insideMargin = {
//     marginLeft: 15
// }

// const alignCenter = {
//     textAlign: 'center'
// }

// const header = {
//     display: 'flex',
//     justifyContent: 'space-between'
// }

// export default withStyles(styles)(ShowDeleted);