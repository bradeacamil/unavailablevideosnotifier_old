import React from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';
import { connect } from 'react-redux';

import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { CircularProgress } from 'material-ui/Progress';
import purple from 'material-ui/colors/purple';
import RefreshIcon from 'material-ui-icons/Refresh';

import { GoogleLogin } from 'react-google-login';

import ShowDeleted from './ShowDeleted';
import { 
    getPlaylistNames, 
    getPlaylistIdsFromYoutube, 
    getTracksFromYoutube 
} from '../utils/helpers';
import { 
    fetchPlaylistsFromYoutube, 
    fetchPlaylistsFromDb,
    fetchPlaylistNames
} from '../actions/playlists';
import { 
    fetchPlaylistItemsFromYoutube, 
    fetchPlaylistItemsFromDb 
} from '../actions/playlistItems';

class Find extends React.Component {
    state = {
        loading: false,
        clickedCompare: false,
        deletedVideos: [],
        channel: ''
    };

    setInitialState = () => {
        const initialState = {
            loading: false,
            clickedCompare: false,
            deletedVideos: [],
            channel: ''
        };
        return initialState;
    }

    componentWillUnmount = () => {
        this.props.dispatch({
            type: 'RESET_STORE'
        });

        this.setState(() => this.setInitialState());
    }

    onChannelChange = (e) => {
        const channel = e.target.value.trim(' ');
        this.setState(() => ({channel}));
    }

    fetch = () => {
        let i = 0;

        getPlaylistNames(this.state.channel)
            .then((result) => this.props.dispatch(fetchPlaylistNames(result)));

        return new Promise((resolve, reject) =>  {
            getPlaylistIdsFromYoutube(this.state.channel)
            .then((result) => {
                this.props.dispatch(fetchPlaylistsFromYoutube(result.sort()));

                this.props.playlistsYoutube.map((playlistId) => {
                    getTracksFromYoutube(playlistId)
                        .then((playlistItems) => {
                            this.props.dispatch(fetchPlaylistItemsFromYoutube(playlistId, playlistItems));

                            i++;
                            if (i === this.props.playlistsYoutube.length)
                                resolve();
                        });
                });
            });
        });
    }

    onCompareClick = () => {
        const tracksYoutube = [];

        this.setState(() => ({ loading: true }));
        
        this.fetch()
            .then(() => {
                this.setState(() => ({ loading: false }));
                this.setState((prevState) => ({ clickedCompare: true }));

                this.getDeletedVideos();
            });

    }

    onNewCompareClick = () => {
        this.props.dispatch({
            type: 'RESET_STORE'
        });

        this.setState(() => this.setInitialState());
    }

    getPlaylistName = (playlistId) => {
        let result = '';
        
        this.props.playlistNames.map((playlist) => {
            if (playlist.playlistId.trim(' ') === playlistId.trim(' '))
                result = playlist.playlistName;
        });

        return result;
    }

    getDeletedVideos = () => {
        this.props.playlistItemsYoutube.map((item) => {
            item.playlistItems.map((track, index, array) => {
                if (!track || track === 'Deleted video' || track === 'Private video') {
                    this.setState((prevState) => ({
                        deletedVideos: [...prevState.deletedVideos, { 
                            playlistId: item.playlistId,
                            previousTrack: array[index - 1],
                            nextTrack: array[index + 1]
                        }]
                    }));
                }
            });
        });
    }

    playlistsWithDeletedVideos = () => {
        let result = [];
        let playlistsWithDeletedVideos = [];

        this.state.deletedVideos.map((playlist) => {
            if (!playlistsWithDeletedVideos.includes(playlist.playlistId))
                playlistsWithDeletedVideos.push(playlist.playlistId);
        });

        return playlistsWithDeletedVideos;
    }

    render() {
        return (
            <div style={container}>
                <div style={center}>
                    {
                        (!this.state.clickedCompare && !this.state.loading) &&
                        <TextField
                            id="name"
                            label="Channel ID"
                            defaultValue='UC-2KsQGXHYO_1uA95pV4VjA'
                            value={this.props.channel}
                            onChange={this.onChannelChange}
                            margin="normal"
                            error={
                                this.state.channel.length > 0 && !this.state.channel.match(/^[A-Za-z0-9_\-]{24}$/)
                            }
                        />
                    }
                    {
                        (!this.state.clickedCompare && !this.state.loading) && 
                        <Button 
                            style={marginButton} 
                            raised color="primary" 
                            onClick={this.onCompareClick}
                            disabled={
                                this.state.channel.length > 0 && !this.state.channel.match(/^[A-Za-z0-9_\-]{24}$/) &&
                                !this.state.clickedCompare && !this.state.loading
                            }
                        >
                            Find
                        </Button>
                    }
                    {
                        this.state.loading ?
                        <div style={center}>
                            <CircularProgress style={{ color: purple[500]}} />
                        </div>
                        :
                        <div>
                        <div style={buttonStyle}>
                            {
                                this.state.clickedCompare && 
                                <Button
                                    fab color="primary" 
                                    aria-label="New search"
                                    onClick={this.onNewCompareClick}    
                                >
                                    <RefreshIcon />
                                </Button>
                            }
                        </div>
                        </div>
                    }
                </div>
                    {
                        <div>
                            {this.state.clickedCompare ?
                                <div style={cardsStyle}>
                                    {                        
                                        this.playlistsWithDeletedVideos().map((playlistId) => 
                                            <ShowDeleted
                                                key={playlistId} 
                                                playlistId={playlistId} 
                                                playlistName={this.getPlaylistName(playlistId)} 
                                                deletedVideos={this.state.deletedVideos}
                                            />
                                        )
                                    }
                                </div>
                                :
                                ''  
                            }   
                        </div>
                    }
            </div>
        );
    }   
};

const mapStateToProps = (state) => {
    return {
        playlistsYoutube: state.playlistsYoutube,
        playlistItemsYoutube: state.playlistItemsYoutube,
        playlistNames: state.playlistNames
    };
};

const center = {
    margin: 'auto'
};

const marginButton = {
    marginLeft: 10
};

const alignCenter = {
    margin: 'auto'
}
const container = {
    display: 'flex',
    flexDirection: 'column',
    height: '500px'
}

const cardsStyle = {
    alignItems: 'stretch'
}

const buttonStyle = {
    position: 'fixed',
	width: '60px',
	height: '60px',
	bottom: '40px',
	right: '60px'
}

export default connect(mapStateToProps)(Find);