import React from 'react';
import ReactDOM from 'react-dom';

import Track from './Track';

const ShowWarning = (props) => (
    <div>           
        <h3>
            <a href={`https://www.youtube.com/playlist?list=${props.playlistId}`}>
                {props.playlistName}
            </a>
        </h3>
        {
            props.warnings.map((track) => {
                if (props.playlistId === track.playlistId) {
                    return <Track key={track.youtubeTrack} youtubeTrack={track.youtubeTrack} dbTrack={track.dbTrack} />
                }
            })
        }
    </div>
);

export default ShowWarning;