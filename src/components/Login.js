import React from 'react';
import ReactDOM from 'react-dom';

import { 
    BrowserRouter, 
    Route, 
    Switch, 
    Link,
    NavLink,
    withRouter
} from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from 'material-ui/styles';

import { GoogleLogin } from 'react-google-login';
import { OldSocialLogin as SocialLogin } from 'react-social-login';
import { GoogleLoginButton, FacebookLoginButton } from 'react-social-login-buttons';
//import GoogleLoginButton from 'react-social-login-buttons/lib/buttons/GoogleLoginButton';

import { login } from '../actions/login';

class Login extends React.Component {
    response = (e) => {
        if(e._profile.id) {
            this.props.dispatch(login(e));
            this.props.history.push('/');
        }
    }

    render() {
        return (
            <div style={container}>
                <div style={centerButtons}>
                    <SocialLogin
                        provider='google'
                        appId="512490168050-1euko2nk5opcrjjlp36nv0pg6jc60b5b.apps.googleusercontent.com"
                        callback={this.response}
                    >
                        <GoogleLoginButton text="Login with Google" />
                    </SocialLogin>
                    <SocialLogin
                        provider='facebook'
                        appId=""
                        callback={this.response}
                    >
                        <FacebookLoginButton text="Login with Facebook" />
                    </SocialLogin>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.isLoggedIn
    };
};

const centerButtons = {
    margin: 'auto'
};

const container = {
    display: 'flex',
    flexDirection: 'column',
    height: '500px'
}
export default connect(mapStateToProps)(withRouter(Login));
// export default withStyles(styles, { withTheme: true })(withRouter(Header));