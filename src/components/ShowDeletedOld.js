import React from 'react';
import ReactDOM from 'react-dom';

import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

import Track from './Track';

const ShowDeleted = (props) => (
    <div style={paddingOutside}>          
        <Paper elevation={4}>
            <div style={paddingInside}>
                <Typography style={alignCenter} type="headline" component="h3">
                    <a href={`https://www.youtube.com/playlist?list=${props.playlistId}`}>
                        '{props.playlistName}'
                    </a> playlist
                </Typography>
                <Typography type="body1" component="p">
                    {
                    props.differences.map((track) => {
                        if (props.playlistId === track.playlistId) {
                            return <Track key={track.previousTrack} previousTrack={track.previousTrack} nextTrack={track.nextTrack} />
                        }
                    })
                }
                </Typography>
            </div>
        </Paper> 
    </div>
);

const paddingOutside = {
    paddingTop: 20,
    paddingRight: 35,
    paddingLeft: 35
}

const paddingInside = {
    padding: 20
}

const alignCenter = {
    textAlign: 'center'
}

export default ShowDeleted;