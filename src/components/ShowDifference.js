import React from 'react';
import ReactDOM from 'react-dom';

import Track from './Track';

const ShowDifference = (props) => (
    <div>              
        <h3>
            <a href={`https://www.youtube.com/playlist?list=${props.playlistId}`}>
                {props.playlistName}
            </a>
        </h3>
        {
            props.differences.map((track) => {
                if (props.playlistId === track.playlistId) {
                    return <Track key={track.deletedTrack} track={track.deletedTrack} />
                }
            })
        }
    </div>
);

export default ShowDifference;