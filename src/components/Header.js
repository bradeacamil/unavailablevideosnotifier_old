import React from 'react';
import ReactDOM from 'react-dom';

import { 
    BrowserRouter, 
    Route, 
    Switch, 
    Link,
    NavLink,
    withRouter
} from 'react-router-dom';
import Button from 'material-ui/Button';
import Menu, { MenuItem } from 'material-ui/Menu';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Drawer from 'material-ui/Drawer';

class Header extends React.Component {
    state = {
        anchorEl: null,
        open: false,
        left: false
    };

    toggleDrawer = (open) => () => {
        this.setState({
            left: open
        });
    };

    handleRequestClose = () => {
        this.setState({ open: false });
    };

    handleClickHome = () => {
        this.setState({ open: false });
        this.props.history.push('/');
    };

    handleClickCompare = () => {
        this.setState({ open: false });
        this.props.history.push('/compare');
    };

    handleClickFind = () => {
        this.setState({ open: false });
        this.props.history.push('/find');
    };

    handleClickAbout = () => {
        this.setState({ open: false});
        this.props.history.push('/about');
    }

    render() {
        return (
            <div style={container}>
                <AppBar position="static">
                    <Toolbar>
                    <IconButton 
                        color="contrast" 
                        aria-label="Menu"
                        onClick={this.toggleDrawer(!this.state.left)}
                    >
                        <MenuIcon />
                    </IconButton>

                    <div style={alignTitleLeft}></div>

                    <div style={alignTitleRight}>
                        <Typography type="body1" color="inherit">
                            Unavailable videos notifier
                        </Typography>
                    </div>

                    <Drawer open={this.state.left} onRequestClose={this.toggleDrawer(false)}>
                        <div
                            tabIndex={0}
                            role="button"
                            onClick={this.toggleDrawer(false)}
                            onKeyDown={this.toggleDrawer(false)}
                        >
                            <MenuItem onClick={this.handleClickHome}>Home</MenuItem>
                            <MenuItem onClick={this.handleClickCompare}>Compare</MenuItem>
                            <MenuItem onClick={this.handleClickFind}>Find</MenuItem>
                            <MenuItem onClick={this.handleClickAbout}>About</MenuItem>
                        </div>
                    </Drawer>
                </Toolbar>
                </AppBar>
            </div>
        );
    }
}

const alignTitleRight = {
    flexDirection: 'row',
    alignItems: 'right',
    justifyContent: 'right'
}

const alignTitleLeft = {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'left',
    justifyContent: 'left'
}

const container = {
    display: 'flex'
}

const styles = {
    list: {
      width: 250,
    },
    listFull: {
      width: 'auto',
    },
  };

export default withRouter(Header);

// import React from 'react';
// import ReactDOM from 'react-dom';

// import { 
//     BrowserRouter, 
//     Route, 
//     Switch, 
//     Link,
//     NavLink,
//     withRouter
// } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import { withStyles } from 'material-ui/styles';
// import classNames from 'classnames';
// import Drawer from 'material-ui/Drawer';
// import AppBar from 'material-ui/AppBar';
// import Toolbar from 'material-ui/Toolbar';
// import List from 'material-ui/List';
// import { MenuItem } from 'material-ui/Menu';
// import Typography from 'material-ui/Typography';
// import TextField from 'material-ui/TextField';
// import Divider from 'material-ui/Divider';
// import IconButton from 'material-ui/IconButton';
// import MenuIcon from 'material-ui-icons/Menu';
// import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
// import ChevronRightIcon from 'material-ui-icons/ChevronRight';

// const drawerWidth = 240;

// const styles = theme => ({
//   root: {
//     width: '100%',
//     height: 430,
//     zIndex: 1,
//     overflow: 'hidden',
//   },
//   appFrame: {
//     position: 'relative',
//     display: 'flex',
//     width: '100%',
//     height: '100%',
//   },
//   appBar: {
//     position: 'absolute',
//     transition: theme.transitions.create(['margin', 'width'], {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//   },
//   appBarShift: {
//     width: `calc(100% - ${drawerWidth}px)`,
//     transition: theme.transitions.create(['margin', 'width'], {
//       easing: theme.transitions.easing.easeOut,
//       duration: theme.transitions.duration.enteringScreen,
//     }),
//   },
//   'appBarShift-left': {
//     marginLeft: drawerWidth,
//   },
//   'appBarShift-right': {
//     marginRight: drawerWidth,
//   },
//   menuButton: {
//     marginLeft: 12,
//     marginRight: 20,
//   },
//   hide: {
//     display: 'none',
//   },
//   drawerPaper: {
//     position: 'relative',
//     height: '100%',
//     width: drawerWidth,
//   },
//   drawerHeader: {
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'flex-end',
//     padding: '0 8px',
//     ...theme.mixins.toolbar,
//   },
//   content: {
//     width: '100%',
//     flexGrow: 1,
//     backgroundColor: theme.palette.background.default,
//     padding: theme.spacing.unit * 3,
//     transition: theme.transitions.create('margin', {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//     height: 'calc(100% - 56px)',
//     marginTop: 56,
//     [theme.breakpoints.up('sm')]: {
//       content: {
//         height: 'calc(100% - 64px)',
//         marginTop: 64,
//       },
//     },
//   },
//   'content-left': {
//     marginLeft: -drawerWidth,
//   },
//   'content-right': {
//     marginRight: -drawerWidth,
//   },
//   contentShift: {
//     transition: theme.transitions.create('margin', {
//       easing: theme.transitions.easing.easeOut,
//       duration: theme.transitions.duration.enteringScreen,
//     }),
//   },
//   'contentShift-left': {
//     marginLeft: 0,
//   },
//   'contentShift-right': {
//     marginRight: 0,
//   },
// });

// class Header extends React.Component {
//     state = {
//         open: false,
//         anchor: 'left',
//     };

//     handleDrawerOpen = () => {
//         this.setState({ open: true });
//     };

//     handleDrawerClose = () => {
//         this.setState({ open: false });
//     };

//     handleChangeAnchor = event => {
//         this.setState({
//         anchor: event.target.value,
//         });
//     };

//     handleClickHome = () => {
//         this.setState({ open: false });
//         this.props.history.push('/');
//     };
    
//     handleClickCompare = () => {
//         this.setState({ open: false });
//         this.props.history.push('/compare');
//     };
    
//     handleClickFind = () => {
//         this.setState({ open: false });
//         this.props.history.push('/find');
//     };

//     render() {
//         const { classes, theme } = this.props;
//         const { anchor, open } = this.state;

//         const drawer = (
//         <Drawer
//             type="persistent"
//             classes={{
//             paper: classes.drawerPaper,
//             }}
//             anchor={anchor}
//             open={open}
//         >
//             <div className={classes.drawerInner}>
//             <div className={classes.drawerHeader}>
//                 <IconButton onClick={this.handleDrawerClose}>
//                 {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
//                 </IconButton>
//             </div>
//             <Divider />
//                 <MenuItem onClick={this.handleClickHome}>Home</MenuItem>
//                 <MenuItem onClick={this.handleClickCompare}>Compare</MenuItem>
//                 <MenuItem onClick={this.handleClickFind}>Find</MenuItem>
//             </div>
//         </Drawer>
//         );

//         let before = null;
//         let after = null;

//         if (anchor === 'left') {
//         before = drawer;
//         } else {
//         after = drawer;
//         }

//         return (
//         <div className={classes.root}>
//             <div className={classes.appFrame}>
//             <AppBar
//                 className={classNames(classes.appBar, {
//                 [classes.appBarShift]: open,
//                 [classes[`appBarShift-${anchor}`]]: open,
//                 })}
//             >
//                 <Toolbar disableGutters={!open}>
//                 <IconButton
//                     color="contrast"
//                     aria-label="open drawer"
//                     onClick={this.handleDrawerOpen}
//                     className={classNames(classes.menuButton, open && classes.hide)}
//                 >
//                     <MenuIcon />
//                 </IconButton>
//                 <Typography type="title" color="inherit" noWrap>
//                     Unavailable videos notifier
//                 </Typography>
//                 </Toolbar>
//             </AppBar>
//             {before}
//             {after}
//             </div>
//         </div>
//         );
//     }
//     }

//     Header.propTypes = {
//     classes: PropTypes.object.isRequired,
//     theme: PropTypes.object.isRequired,
// };

// export default withStyles(styles, { withTheme: true })(withRouter(Header));