import { createStore, combineReducers } from 'redux';

import playlistsYoutubeReducer from '../reducers/playlistsYoutube';
import playlistsDbReducer from '../reducers/playlistsDb';
import playlistItemsYoutubeReducer from '../reducers/playlistItemsYoutube';
import playlistItemsDbReducer from '../reducers/playlistItemsDb';
import playlistNamesReducer from '../reducers/playlistNames';
import loginReducer from '../reducers/login';

export default () => {
    const store = createStore(
        combineReducers({
            playlistsYoutube: playlistsYoutubeReducer,
            playlistsDb: playlistsDbReducer,
            playlistItemsYoutube: playlistItemsYoutubeReducer,
            playlistItemsDb: playlistItemsDbReducer,
            playlistNames: playlistNamesReducer,
            isLoggedIn: loginReducer
        }),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    return store;
}