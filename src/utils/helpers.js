import axios from 'axios';

let playlistIds;
let tracks = [];
let tracksMerged;

export let getPlaylistIdsFromDb = (channelId) => {
    let getPlaylistIdsFromDbUrl = `https://luq89c36p9.execute-api.eu-central-1.amazonaws.com/prod/getPlaylistIdsFromDb?channelid=${channelId}`;

    return new Promise((resolve, reject) =>  {
        axios.get(getPlaylistIdsFromDbUrl)
            .then( (response) => {
                playlistIds = response.data;
            
                resolve(playlistIds);
            })
            .catch( (e) => 
                console.log('Error when getting the playlist IDs: ', e) );
        }
    );
}

export let getPlaylistIdsFromYoutube = (channelId) => {
    let getPlaylistIdsFromYoutubeUrl = `https://b6ej01sgzd.execute-api.eu-central-1.amazonaws.com/prod/getPlaylistIdsFromYoutube?channelid=${channelId}`;

    return new Promise((resolve, reject) =>  {
        axios.get(getPlaylistIdsFromYoutubeUrl)
            .then( (response) => {
                playlistIds = response.data;

                resolve(playlistIds);
            })
            .catch( (e) => 
                console.log('Error when getting the playlist IDs: ', e) );
        }
    );
}

export let getPlaylistNames = (channelId) => {
    let getPlaylistNameUrl = `https://m52od0n1m6.execute-api.eu-central-1.amazonaws.com/prod/getPlaylistName?channelid=${channelId}`;

    return new Promise((resolve, reject) =>  {
        axios.get(getPlaylistNameUrl)
            .then( (response) =>
                resolve(response.data))
            .catch( (e) => 
                console.log(e));
    });
}

export let getTracksFromDb = (playlistId) => {
    let getTracksFromDbUrl = `https://wvz4f91khh.execute-api.eu-central-1.amazonaws.com/prod/getTracksFromDb?playlistid=${playlistId}`;

    return new Promise((resolve, reject) =>  {
        axios.get(getTracksFromDbUrl)
            .then( (response) => {
                tracks = [];
                tracksMerged = [];

                tracks.push(response.data);
                tracksMerged = [].concat.apply([], tracks);   

                resolve(tracksMerged);   
            })
            .catch( (e) => 
                console.log('Error when getting the tracks: ', e) );   
        }
    );
}

export let getTracksFromYoutube = (playlistId) => {
    let getTracksFromYoutubeUrl = `https://1l9wtrbit5.execute-api.eu-central-1.amazonaws.com/prod/getTracksFromYoutube?playlistid=${playlistId}`;

    return new Promise((resolve, reject) =>  {
        axios.get(getTracksFromYoutubeUrl)
            .then( (response) => {
                tracks = [];
                tracksMerged = [];

                tracks.push(response.data);
                tracksMerged = [].concat.apply([], tracks);   

                resolve(tracksMerged);   
            })
            .catch( (e) =>
                console.log('Error when getting the tracks: ', e) );
        }
    );
}

export let writePlaylistsToDb = (channelId, playlists) => {
    let writePlaylistsToDbUrl = `https://p6v57c1lqk.execute-api.eu-central-1.amazonaws.com/prod/writeplaylistids?channelid=${channelId}`;

    axios.post(writePlaylistsToDbUrl, {
        "playlists": playlists
    })
        .then( (response) => response.data )
        .catch( (e) => e );
}

export let writeTracksToDb = (playlist, tracks) => {
    let writeTracksToDbUrl = 'https://nbumpmati3.execute-api.eu-central-1.amazonaws.com/prod/writeTracksToDb';

    axios.post(writeTracksToDbUrl, {
        "tracks": tracks,
        "playlist": playlist
    })
        .then( (response) => console.log('writeTracksToDb: ', response.data) )
        .catch( (e) => console.log('Error when writing tracks to the DB: ', e) );
}