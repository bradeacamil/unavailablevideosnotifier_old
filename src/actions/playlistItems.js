export const fetchPlaylistItemsFromYoutube = (playlistId, playlistItems) => ({
    type: 'FETCH_PLAYLIST_ITEMS_FROM_YOUTUBE',
    playlistId,
    playlistItems
});

export const fetchPlaylistItemsFromDb = (playlistId, playlistItems) => ({
    type: 'FETCH_PLAYLIST_ITEMS_FROM_DB',
    playlistId,
    playlistItems
});