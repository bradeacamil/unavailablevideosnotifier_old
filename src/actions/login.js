export const login = (token) => ({
    type: 'IS_LOGGED_IN',
    token
});