export const fetchPlaylistsFromYoutube = (playlistsYoutube) => ({
    type: 'FETCH_PLAYLISTS_FROM_YOUTUBE',
    playlistsYoutube
});

export const fetchPlaylistsFromDb = (playlistsDb) => ({
    type: 'FETCH_PLAYLISTS_FROM_DB',
    playlistsDb
});

export const fetchPlaylistNames = (playlistNames) => ({
    type: 'FETCH_PLAYLIST_NAMES',
    playlistNames
});