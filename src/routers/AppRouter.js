import React from 'react';
import ReactDOM from 'react-dom';
import { 
    BrowserRouter, 
    Route, 
    Switch, 
    Link,
    NavLink 
} from 'react-router-dom';

import Header from '../components/Header';
import Home from '../components/Home';
import Compare from '../components/Compare';
import About from '../components/About';
import Find from '../components/Find';
import Login from '../components/Login';

const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Header />
            <Switch>
                <Route path="/" component={Home} exact={true} />
                <Route path="/compare" component={Compare} />
                <Route path="/find" component={Find} />
                <Route path="/about" component={About} />
                <Route path="/login" component={Login} />
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;